# V2Migration #

[TOC]

## What is this repository for? ##

To provide the scripts required to allow a mapping of URLs between Movehut V2 and Movehut v3 so as to minimise the loss of SEO link equity.

Both Movehut V2 and Movehut V3 URLs are based on geography

* V2 = Town and City (though city is a misnomer as it tends to contain some form of county)
* V3 = Town and County

In 85% of cases there is a direct match between V2 Town/City and V3 Town/County

By matching V2 and V3 on town and then matching  the city/county within a 5 mile box another 6% of locations can be matched.

The artifacts in this repository provide the means of satisfying that match.

## Script files ##

### Bash script files ###

**V2MigrationInstall.sh** runs the MySQL scripts listed in order as displayed in the table below.

**WARNING:-** 
 
The deployment database and credentials are parameterised at the top of the script.

A better method of handling such items is required.  This approach has been used purely to support a POC and exposing credentials in this manner for production purposes must not happen.



### SQL Scripts ###

|SQL File                      |Purpose|
|:-----------------------------||
|00_CreateV2Locations_DB.sql   |Drops and recreates the empty V2Location database|
|01_CreateV2MigrationTables.sql|Creates the table structures sourced from the V2 database and the derived V2Location structure equivalent of the V3 town table|
|02_CreateReferenceData.sql    |Populates the V2 tables with the exception of tbl_locations as these are the tables on which tbl_locations depends|
|02_CreateLocationData.sql     |Populates the V2 tbl_locations table|
|03_CreateViews.sql            |Creates a view that presents V2 data in the same format as the V3 town table|
|04_PopulateV2Location.sql     |Materialises the view created above as the V2Location table|
|05_CreateIndexes.sql          |Indexes the V2Location table|
|06_CreateV3Town.sql           |Creates the V3Geography table which is a copy of the V3 town table minus indices that are of not used for the purposes of matching|
|07_DeriveGeographyTables.sql  |Creates tables that provide a geographic mid-point for various units of geography derived from the V3Geography table|
|08_CreateV2V3MappingTable.sql |Creates and populates mapping tables for when there is no direct equivalent in V3 of the V2 data used to create the V2 URLs|

## Database Objects ##

### Documentation ###

MySQL allows tables and columns to be have a comment attached to them.  As part of the migration the COMMENT property has been set using the following practise:-

*  All tables, regardless of origin have had their COMMENT property set.
*  For fields in new tables the COMMENT property has been set.
*  For fields in legacuy tables the COMMENT property has NOT been set.

For table documentation the following query will retrieve the required information

```sql
SELECT TABLE_NAME,TABLE_COMMENT
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = DATABASE();
```

For field\column information the following query will retrieve the field name, type and documentation.

```sql
SELECT 
    TABLE_NAME,
    COLUMN_NAME,
    CONCAT(DATA_TYPE,
            CASE DATA_TYPE
                WHEN 'varchar' THEN CONCAT('(', CHARACTER_MAXIMUM_LENGTH, ')')
                WHEN 'char' THEN CONCAT('(', CHARACTER_MAXIMUM_LENGTH, ')')
                WHEN 'decimal' THEN CONCAT('(',NUMERIC_PRECISION,',',NUMERIC_SCALE,')')
                ELSE ''
            END) AS DataType,
    COLUMN_COMMENT
FROM
    INFORMATION_SCHEMA.COLUMNS AS C
WHERE
    TABLE_SCHEMA = DATABASE();

```


### Tables and Views ###

| TableName                | Description      |
|:------------------------ | |
| tbl_city       | Sourced from V2 tbl_city.  This is a misnomer that frequently provides the county URL part of the V2 city/county URL |
| tbl_county     | Sourced from V2 tbl_county.  Not actually used for mapping purposes but included for data investigation |
| tbl_geocodes   | Sourced from V2 tbl_geocodes. Provides a geographic co-ordinate to be attached to a tbl_location record. |
| tbl_locations  | The V2.tbl_locations table acts as a central hub to bind the various V2 geography tables together |
| tbl_town       | Sourced from V2 tbl_town.  Provides the URL part that indicates the town. |
| V2Town         | VIEW used to present geography data held in the V2 tables in the same format as the V3 town table            |
| V2Location     | Materialised version of the V2Town view.|
| V2V3Mapping_town_county | Holds the mapping between the old V2 Town/City and the new V3 Town/County where the V2 City and V3 County are different |
| V2V3Mapping_town_county_match | Where an exact match can be found between the V2 town/city and V3 town/county this table contains the ids from those matches |
| V3Geography    | Exact copy of the V3 town table |
| Midpoint_county | The mid point for the county as derived from V3Geography |
| Midpoint_PostalSector | The mid point for the postal sector as derived from V3Geography |
| Midpoint_town_county | The mid point for the town/county combination as derived from V3Geography |