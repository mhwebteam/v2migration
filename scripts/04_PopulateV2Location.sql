/*
	04_PopulateV2Location.sql
    
*/
TRUNCATE TABLE V2Location;

INSERT INTO V2Location(location_id,safe_name,safe_county,latitude,longitude)
SELECT location_id,safe_name,safe_county,latitude,longitude 
FROM V2Town;

