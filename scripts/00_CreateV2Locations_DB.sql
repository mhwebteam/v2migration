/*
	00_CreatePostalAddress_DB.SQL
	=============================
    Creates an empty V2Locations database to hold the original five V2 location tables:-
    1.	tbl_city
    2.	tbl_county
    3.	tbl_geocodes
    4.	tbl_locations
    5.	tbl_town
*/
DROP DATABASE IF EXISTS V2Location;

CREATE DATABASE IF NOT EXISTS V2Location COLLATE utf8_unicode_ci;


