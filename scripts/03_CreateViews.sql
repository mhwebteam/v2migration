/*
	03_CreateViews.SQL
    =======================
    Views are used to present the V2 data in a form that is useful for the transition to V3
*/

CREATE OR REPLACE VIEW V2Town
AS
SELECT 
	L.location_id,
	REPLACE(T.town_sql,'-',' ') AS safe_name,
	REPLACE(C.city_sql,'-',' ') AS safe_county,
	G.latitude,
	G.longitude
FROM tbl_locations AS L
	INNER JOIN tbl_city AS C
    ON L.city_id = C.city_id
    INNER JOIN tbl_town AS T
    ON L.town_id = T.town_id
    LEFT JOIN tbl_geocodes AS G
    ON L.geocode_id = G.geocode_id;
    
    