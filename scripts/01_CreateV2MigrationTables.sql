# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: perconadb.movehut.co.uk (MySQL 5.6.30-76.3-log)
# Database: movehut_web
# Generation Time: 2016-07-04 12:46:04 +0000
# ************************************************************



# Dump of table tbl_city
# ------------------------------------------------------------

CREATE TABLE tbl_city (
  city_id int(11) NOT NULL ,
  city varchar(60) NOT NULL,
  city_sql varchar(60) DEFAULT NULL,
  original_id int(11) DEFAULT NULL,
  PRIMARY KEY (city_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Sourced from V2 tbl_city.  This is a misnomer that frequently provides the county URL part of the V2 city/county URL';



# Dump of table tbl_county
# ------------------------------------------------------------

CREATE TABLE tbl_county (
  county_id int(11) NOT NULL ,
  county varchar(60) NOT NULL,
  county_sql varchar(60) DEFAULT NULL,
  original_id int(11) DEFAULT NULL,
  PRIMARY KEY (county_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Sourced from V2 tbl_county.  Not actually used for mapping purposes but included for data investigation';



# Dump of table tbl_geocodes
# ------------------------------------------------------------

CREATE TABLE tbl_geocodes (
  geocode_id int(11) NOT NULL ,
  longitude decimal(10,7) DEFAULT NULL,
  latitude decimal(10,7) DEFAULT NULL,
  grid_reference varchar(45) DEFAULT NULL,
  geo_reason varchar(45) DEFAULT NULL,
  PRIMARY KEY (geocode_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Sourced from V2 tbl_geocodes. Provides a geographic co-ordinate to be attached to a tbl_location record.';



# Dump of table tbl_town
# ------------------------------------------------------------

CREATE TABLE tbl_town (
  town_id int(11) NOT NULL ,
  town varchar(60) NOT NULL,
  town_sql varchar(60) DEFAULT NULL,
  original_id int(11) DEFAULT NULL,
  PRIMARY KEY (town_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Sourced from V2 tbl_town.  Provides the URL part that indicates the town.';


# Dump of table tbl_locations
# ------------------------------------------------------------

CREATE TABLE tbl_locations (
  location_id int(11) NOT NULL ,
  town_id int(11) DEFAULT NULL,
  city_id int(11) DEFAULT NULL,
  county_id int(11) DEFAULT NULL,
  short_postcode_id int(11) DEFAULT NULL,
  full_postcode_id int(11) DEFAULT NULL,
  geocode_id int(11) DEFAULT NULL,
  country_code int(11) DEFAULT NULL,
  population int(11) DEFAULT NULL,
  geo_processed tinyint(1) NOT NULL DEFAULT '0',
  geo_failure_reason smallint(6) DEFAULT NULL,
  search_radius decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (location_id),
  KEY fk_tbl_locations_tbl_geocodes1 (geocode_id),
  KEY fk_tbl_locations_tbl_town1 (town_id),
  KEY fk_tbl_locations_tbl_city1 (city_id),
  KEY fk_tbl_locations_tbl_county1 (county_id),
  CONSTRAINT fk_tbl_locations_tbl_city1 FOREIGN KEY (city_id) REFERENCES tbl_city (city_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_tbl_locations_tbl_county1 FOREIGN KEY (county_id) REFERENCES tbl_county (county_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_tbl_locations_tbl_geocodes1 FOREIGN KEY (geocode_id) REFERENCES tbl_geocodes (geocode_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_tbl_locations_tbl_town1 FOREIGN KEY (town_id) REFERENCES tbl_town (town_id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='The V2.tbl_locations table acts as a central hub to bind the various V2 geography tables together';


CREATE TABLE V2Location (
    location_id INT NOT NULL COMMENT 'The surrogate key from the V2 location table' PRIMARY KEY,
    safe_name VARCHAR(60) NOT NULL COMMENT 'The cleansed town name',
    safe_county VARCHAR(60) NOT NULL COMMENT 'The cleansed county name',
    latitude DECIMAL(10 , 7 ) NULL COMMENT 'The latitude portion of the grid reference',
    longitude DECIMAL(10 , 7 ) NULL COMMENT 'The longitude portion of the grid reference'
) COMMENT='Location data from Movehut V2 presented in the same format as the V3.town table.';




