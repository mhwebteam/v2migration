/*
	05_CreateIndexes.sql
*/
SET @exist := (SELECT COUNT(*) FROM information_schema.statistics WHERE table_name = 'V2Location' AND index_name = 'idx_Location_towncounty' AND table_schema = DATABASE());
SET @sqlstmt := IF( @exist > 0, 'select ''INFO: Index already exists.''', 'create index idx_Location_towncounty on V2Location(safe_name,safe_county)');
PREPARE stmt FROM @sqlstmt;
EXECUTE stmt;