#! /bin/bash

USER_NAME="DAVE"
PWORD="W@ll4c3"
DB_NAME="V2Location"

echo $USER_NAME

mysql -u$USER_NAME -p$PWORD< 00_CreateV2Locations_DB.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 01_CreateV2MigrationTables.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 02_CreateReferenceData.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 02_CreateLocationData.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 03_CreateViews.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 04_PopulateV2Location.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 05_CreateIndexes.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 06_CreateV3Town.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 07_DeriveGeographyTables.sql
mysql -u$USER_NAME -p$PWORD -D$DB_NAME< 08_CreateV2V3MappingTable.sql
