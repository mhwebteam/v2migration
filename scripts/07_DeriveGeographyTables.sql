/*
	07_DeriveGeographyTables.sql
    ============================
    
*/
DROP TABLE IF EXISTS Midpoint_town_county;
DROP TABLE IF EXISTS Midpoint_county;
DROP TABLE IF EXISTS Midpoint_PostalSector;

CREATE TABLE Midpoint_town_county(
	Town VARCHAR(40) NOT NULL 
		COMMENT "Derived from the V3Geography.name field which in turn was the town.name field from the V3 database",
    County VARCHAR(70) NOT NULL 
		COMMENT "Generally the county but occassionaly a genereal geographic area",
    Easting INT NOT NULL 
		COMMENT "The calculated central point Easting calculated as the half way point between the MIN & MAX Easting for the geography unit",
    Northing INT NOT NULL  
		COMMENT "The calculated central point Northing calculated as the half way point between the MIN & MAX Northing for the geography unit",
    Latitude DECIMAL(10,7) NULL  
		COMMENT "The calculated central point Latitude calculated as the half way point between the MIN & MAX Latitude for the geography unit",
    Longitude DECIMAL(10,7) NULL 
		COMMENT "The calculated central point Longitude calculated as the half way point between the MIN & MAX Longitude for the geography unit",
	MilesAcrossWidestPoint DECIMAL(5,1) NOT NULL
		COMMENT "Where there is more than one latitude/longitude point this will be the straight line distance between the bottom left and top right point.",
	PRIMARY KEY (Town,County),
    KEY idx_Midpoint_town_county_county(County)
) COMMENT='The mid point for the town/county combination as derived from V3Geography';

CREATE TABLE Midpoint_county(
    County VARCHAR(70) NOT NULL 
		COMMENT "Generally the county but occassionaly a genereal geographic area",
    Easting INT NOT NULL 
		COMMENT "The calculated central point Easting calculated as the half way point between the MIN & MAX Easting for the geography unit",
    Northing INT NOT NULL  
		COMMENT "The calculated central point Northing calculated as the half way point between the MIN & MAX Northing for the geography unit",
    Latitude DECIMAL(10,7) NULL  
		COMMENT "The calculated central point Latitude calculated as the half way point between the MIN & MAX Latitude for the geography unit",
    Longitude DECIMAL(10,7) NULL 
		COMMENT "The calculated central point Longitude calculated as the half way point between the MIN & MAX Longitude for the geography unit",
	MilesAcrossWidestPoint DECIMAL(5,1) NOT NULL
		COMMENT "Where there is more than one latitude/longitude point this will be the straight line distance between the bottom left and top right point.",
	PRIMARY KEY (County)
) COMMENT='The mid point for the county as derived from V3Geography';

CREATE TABLE Midpoint_PostalSector(
    PostalSector VARCHAR(6) NOT NULL 
		COMMENT "the postal sector as imported from the V3Geography.postcode field which in turn comes from the V3 town.postcode field",
    Easting INT NOT NULL 
		COMMENT "The calculated central point Easting calculated as the half way point between the MIN & MAX Easting for the geography unit",
    Northing INT NOT NULL  
		COMMENT "The calculated central point Northing calculated as the half way point between the MIN & MAX Northing for the geography unit",
    Latitude DECIMAL(10,7) NULL  
		COMMENT "The calculated central point Latitude calculated as the half way point between the MIN & MAX Latitude for the geography unit",
    Longitude DECIMAL(10,7) NULL 
		COMMENT "The calculated central point Longitude calculated as the half way point between the MIN & MAX Longitude for the geography unit",
	MilesAcrossWidestPoint DECIMAL(5,1) NOT NULL
		COMMENT "Where there is more than one latitude/longitude point this will be the straight line distance between the bottom left and top right point.",
	PRIMARY KEY (PostalSector)
) COMMENT='The mid point for the postal sector as derived from V3Geography';


INSERT INTO Midpoint_town_county (
	Town,
    County,
    Easting,
    Northing,
    Latitude,
    Longitude,
    MilesAcrossWidestPoint
)
SELECT 
	safe_name,
    safe_county,
    (MAX(Easting)+MIN(Easting))/2 AS MidEasting,
    (MAX(Northing)+MIN(Northing))/2 AS MidNorthing,
    (MAX(Latitude)+MIN(Latitude))/2 AS MidLatitude,
    (MAX(Longitude)+MIN(Longitude))/2 AS MidLongitude,
	SQRT(POWER(ABS(MAX(Latitude) - MIN(Latitude))*69,2)+
    POWER(ABS(MAX(Longitude) - MIN(Longitude))*41,2)) AS CrudeDistance
    
FROM V3Geography
GROUP BY safe_name,safe_county;

INSERT INTO Midpoint_county (
    County,
    Easting,
    Northing,
    Latitude,
    Longitude,
    MilesAcrossWidestPoint
)
SELECT 
    safe_county,
    (MAX(Easting)+MIN(Easting))/2 AS MidEasting,
    (MAX(Northing)+MIN(Northing))/2 AS MidNorthing,
    (MAX(Latitude)+MIN(Latitude))/2 AS MidLatitude,
    (MAX(Longitude)+MIN(Longitude))/2 AS MidLongitude,
	SQRT(POWER(ABS(MAX(Latitude) - MIN(Latitude))*69,2)+
    POWER(ABS(MAX(Longitude) - MIN(Longitude))*41,2)) AS CrudeDistance
    
FROM V3Geography
GROUP BY safe_county;

INSERT INTO Midpoint_PostalSector (
    PostalSector,
    Easting,
    Northing,
    Latitude,
    Longitude,
    MilesAcrossWidestPoint
)
SELECT 
    postcode,
    (MAX(Easting)+MIN(Easting))/2 AS MidEasting,
    (MAX(Northing)+MIN(Northing))/2 AS MidNorthing,
    (MAX(Latitude)+MIN(Latitude))/2 AS MidLatitude,
    (MAX(Longitude)+MIN(Longitude))/2 AS MidLongitude,
	SQRT(POWER(ABS(MAX(Latitude) - MIN(Latitude))*69,2)+
    POWER(ABS(MAX(Longitude) - MIN(Longitude))*41,2)) AS CrudeDistance
    
FROM V3Geography
GROUP BY postcode;
