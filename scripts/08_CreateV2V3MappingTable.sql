/*
	08_CreateV2V3MappingTable.sql
    ============================
	This script produces two tables
    
		V2V3Mapping_town_county_match
		=============================
        The table records the ids from both V2 and V3 where there is an exact match between the two generations
        on both town and city/county respectively.
        The purpose of this table is to act as an exclusion table for any proximity match entries.
        
        V2V3Mapping_town_county
        =======================
        The table holds the mapping between the V2 town/city and the V3 town/county where the town has an exact match
        but the county has to be derived from a latitude/longitude proximity.
        
        In some cases there are two separate "towns" in close proximity to each other.  This is because the value in the town field
        is not a post town but a either a dependant or double dependant locality.
*/

SET @Radius=5;


DROP TABLE IF EXISTS V2V3Mapping_town_county;
DROP TABLE IF EXISTS V2V3Mapping_town_county_match;

CREATE TABLE V2V3Mapping_town_county(
	V3_Id INT NOT NULL
		COMMENT "The id field from the V3 `town` table.  In some cases an old location will match to more than one new location therefore this is not the primary key",
	Town VARCHAR(60) NOT NULL 
		COMMENT "Derived from the V3Geography.name field which in turn was the town.name field from the V3 database",
    OriginalCityCounty VARCHAR(60) NOT NULL 
		COMMENT "From Movehut V2. Generally the county but occassionaly a genereal geographic area",
    NewCityCounty VARCHAR(40) NOT NULL 
		COMMENT "From Movehut V3. Generally the county but occassionaly a genereal geographic area",
	KEY idx_V2V3Mapping_town_county_Orig(Town,OriginalcityCounty)
) COMMENT='Holds the mapping between the old V2 Town/City and the new V3 Town/County where the V2 City and V3 County are different';

CREATE TABLE V2V3Mapping_town_county_match(
	V2_LocationID INT NOT NULL
		COMMENT "The original V2 tbl_locations.location_id value",
	V3_ID INT NOT NULL
		COMMENT "The V3 town.id value",
	PRIMARY KEY (V2_LocationID,V3_ID)
) COMMENT='Where an exact match can be found between the V2 town/city abnd V3 town/county this table contains the ids from those matches';

INSERT INTO V2V3Mapping_town_county_match (V2_LocationID,V3_ID)
SELECT DISTINCT V2.location_id, V3.id
FROM V2Location AS V2
    INNER JOIN V3Geography AS V3 
    ON V2.safe_name = V3.safe_name
    AND V2.safe_county = V3.safe_county
WHERE V2.safe_county IS NOT NULL;
   

INSERT INTO V2V3Mapping_town_county (
	v3_Id,
    Town,
    OriginalCityCounty,
    NewCityCounty
)
SELECT DISTINCT
    id, 
    safe_name, 
    OriginalCountyOrCity, 
    NewCountyOrCity
FROM
    (SELECT 
        V2.location_id,
            V2.safe_name,
            TRIM(V2.safe_county) AS OriginalCountyOrCity,
            TRIM(V3.safe_county) AS NewCountyOrCity,
            V3.id
    FROM
        V2Location AS V2
    INNER JOIN V3Geography AS V3 ON V2.safe_name = V3.safe_name
    WHERE
        V2.safe_county <> V3.safe_county
            AND ABS(V2.Latitude - V3.Latitude) < @Radius / 111
            AND ABS(V2.Longitude - V3.Longitude) < @Radius / 67) AS DT
            
	# Exclude those V2 locations that have already been mapped.
    LEFT JOIN V2V3Mapping_town_county_match AS Exc
    ON DT.location_id = Exc.V2_LocationID
WHERE Exc.V2_LocationID IS NULL
;

